<?php
/**
 * Created by PhpStorm.
 * User: 15241
 * Date: 30/11/2017
 * Time: 9:40
 */

include "code/helpers.php";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <fieldset>
        <legend>Account info</legend>
        <div>
            <label for="FirstName">First name <span>*</span></label>
            <input type="text" name="FirstName" id="FirstName" temp value="<?php echo $firstName; ?>">
            <span class="error"><?php echo $modelState["FirstNameRequired"]; ?></span>
        </div>
        <div>
            <label for="LastName">Last name <span>*</span></label>
            <input type="text" name="LastName" id="LastName" temp value="<?php echo $lastName; ?>">
            <span class="error"><?php echo $modelState["LastNameRequired"]; ?></span>
        </div>

        <div>
            <label for="Email">E-mail <span>*</span></label>
            <input type="Email" name="Email" id="Email" temp value="<?php echo $email; ?>">
            <span class="error"><?php echo $modelState["EmailValid"]; ?></span>
            <span class="error"><?php echo $modelState["EmailRequired"]; ?></span>
        </div>
        <div>
            <label for="Password">Password <span>*</span></label>
            <input type="password" name="Password" id="Password" temp>
            <span class="error"><?php echo $modelState["PasswordRequired"]; ?></span>
            <span class="error"><?php echo $modelState["PasswordValid"]; ?></span>

        </div>
        <div>
            <label for="ConfirmPassword">Confirm password <span>*</span></label>
            <input type="password" name="ConfirmPassword" id="confirmPassword" temp>
            <span class="error"><?php echo $modelState["ConfirmPasswordRequired"]; ?></span>
            <span class="error"><?php echo $modelState["PasswordValid"]; ?></span>
        </div>
    </fieldset>
    <fieldset>
        <legend>Adress data</legend>
        <div>
            <label for="Address1">Address 1 <span>*</span></label>
            <input type="text" name="Address1" id="Adress1" temp value="<?php echo $address1; ?>">
            <span class="error"><?php echo $modelState["Address1Required"]; ?></span>
        </div>
        <div>
            <label for="Address2">Address 2</label>
            <input type="text" name="Address2" id="Address2" value="<?php echo $address2; ?>">
        </div>
        <div>
            <label for="City">City <span>*</span></label>
            <input type="text" name="City" id="City" temp value="<?php echo $city; ?>">
            <span class="error"><?php echo $modelState["CityRequired"]; ?></span>
        </div>
        <div>
            <label for="PostalCode">Postal code <span>*</span></label>
            <input type="text" name="PostalCode" id="PostalCode" temp value="<?php echo $postalCode; ?>">
            <span class="error"><?php echo $modelState["PostalCodeRequired"]; ?></span>
        </div>
        <div>
            <label for="Country">Country <span>*</span></label>
            <select name="Country" id="Country">
                <option value="Be"  <?php echo $country == 'Be' ? 'selected' : ''; ?> >Belgium</option>
                <option value="Nl"  <?php echo $country == 'Nl' ? 'selected' : ''; ?> >Netherlands</option>
                <option value="Tr"  <?php echo $country == 'Tr' ? 'selected' : ''; ?> >Turkey</option>
            </select>
            <span class="error"><?php echo $modelState["CountryRequired"]; ?></span>
        </div>
    </fieldset>
    <fieldset>
        <legend>Personal data</legend>
        <div>
            <label for="Mobile">Mobile</label>
            <input type="text" name="mobile" id="mobile" value="<?php echo $mobile; ?>">
        </div>
        <div>
            <label for="BirthDay">Date of birth</label>
            <input type="date" name="BirthDay" id="BirthDay" value="<?php echo $birthDay; ?>">
        </div>
        <div>
            <label for="Satisfied">How satisfied are you?</label>
            <input type="range" name="Satisfied" id="satisfied" value="<?php echo $satisfied; ?>">
        </div>
        <div>
            <label for="Course">Department</label>
            <select name="Course" id="Course">
                <option value="hboi" <?php echo $course == 'hboi' ? 'selected' : ''; ?> >HBO 5 IT</option>
                <option value="hboa" <?php echo $course == 'hboa' ? 'selected' : ''; ?> >HBO Accounting</option>
                <option value="hbos" <?php echo $module == 'hbos' ? 'selected' : ''; ?> >HBO 5 Store management</option>
            </select>
        </div>
        <div>
            <label for="Module">Module</label>
            <select name="Module" id="Module">
                <option <?php echo $module == 'A5' ? 'selected' : ''; ?> value="A5">Programming 1</option>
                <option <?php echo $module == 'A6' ? 'selected' : ''; ?> value="A6">Programming 2</option>
                <option <?php echo $module == 'B2' ? 'selected' : ''; ?> value="B2">Programming 3</option>
            </select>
        </div>
        <div>
            <input type="checkbox" name="PHP" value="PHP" id="PHP" <?php echo $php == 'PHP' ? 'checked' : ''; ?> >
            <label for="PHP">PHP</label>
            <input type="checkbox" name="JavaScript"
                   value="JavaScript" <?php echo $javaScript == 'JavaScript' ? 'checked' : ''; ?> >
            <label for="JS">JavaScript</label>
            <input type="checkbox" name="Java" value="Java" <?php echo $java == 'Java' ? 'checked' : ''; ?> >
            <label for="Java">Java</label>
        </div>
    </fieldset>
    <button type="submit" value="register" name="action">Submit</button>
</form>
</body>
</html>
