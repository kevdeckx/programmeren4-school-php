<aside>
    <?php
    $notices = $model->getBoard();
    if (count($notices > 0))
	{ ?>
        <table>
            <tr>
                <th>Select</th>
                <th>Onderwerp</th>
                <th>Code</th>
            </tr>
        <?php
    
        foreach($notices as $notice)
        { ?>
            <tr>
            <td>
                <a href="?uc=Notice/ReadingOne/<?php echo $notice->getId();?>">--></a>
            </td>
            <td><?php echo $notice->getSubject()?></td>
            <td><?php echo $notice->getCode()?></td>
            </tr>
        <?php
        } 
        ?>
    </table>
    <?php 
	} else { ?>
	<p>Geen notities gevonden.</p>
	<?php
	} ?>
</aside>