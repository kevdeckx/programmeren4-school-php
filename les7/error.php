<?php

function checkPositiveInteger($number){
    if ($number < 0) {
          throw new Exception("Getal moet groter zijn dan nul");
    }
}

function checkInteger($number){
        if (!filter_var($number, FILTER_VALIDATE_INT)) {
         throw new Exception("Je moet een geheel getal intypen");
    }
}

if (isset($_POST['Submit'])) {
    try{
        checkPositiveInteger($_POST['Number']);
        checkInteger($_POST['Number']);
    }
    catch(Exception $e){
        echo $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fout afhandeling</title>
</head>
<body>
    <form method="post">
        <div>
        <label for=Number"">Geef een getal in</label>
        <input type="text" name="Number" id="Number"/>
        </div>
        <input type="submit" value="Submit" name="Submit"/>
    </form>
    
</body>
</html>