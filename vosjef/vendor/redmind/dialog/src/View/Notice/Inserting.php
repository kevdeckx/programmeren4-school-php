<header class="flex-container">
    <div class="flex-item-1 align-content-left">
        <a class="home" href="?uc=Home/Index">Home</a>
    </div>
    <div class="flex-item-1 align-content-right">
        <h1>Notice Index</h1>
    </div>
</header>
<main class="show-room flex-container">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="flex-item-4 align-content-left">
                <h2>Notice</h2>
            </div>
            <div class="flex-item-1 align-content-right">
           
                <button type="submit" name="uc" value="Notice/Insert" id="submit" form="insertingForm">
                    Insert
                </button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="insertingForm">
                <div class="field-container">
                    <label for="Subject">Subject</label>
                    <input type="text" name="Subject" required/>
                </div>
                <div class="field-container">
                    <label for="Code">Code</label>
                    <input type="text" name="Code"/>
                </div>
                <div class="field-container">
                    <label for="Message">Message</label>
                    <input type="text" name="Message"/>
                </div>
                <div class="field-container">
                    <label for="Start">Start</label>
                    <input type="date" name="Start"/>
                </div>
                <div class="field-container">
                    <label for="End">End</label>
                    <input type="date" name="End"/>
                </div>
                <div class="field-container">
                    <label for="Source">Source</label>
                    <input type="text" name="Source"/>
                </div>
                <button type="submit" name="uc" value="Notice/Insert">Insert</button>
            </form>
            <div class="feedback"></div>
        </div>
    </div>
    <div class="flex-item-1">
        <aside class="list">
            <!-- partial view lijst notities -->
        </aside>
    </div>

</main>
<script type="text/javascript">
    // window.onload = function () {
    //     var submit = document.getElementById("submit");
        
        
    //     function submitForm(){
    //               document.getElementById("insertingForm").submit()
            
    //     }
    //     submit.addEventListener("click", submitForm);

    // }
</script>


