<?php
session_start();
$productList = array();
if (isset($_POST['Submit'])) {
    if (isset($_SESSION['Products'])) {
        $productList = $_SESSION['Products'];
    }
    $productList[] = $_POST['Product'];
    $_SESSION['Products'] = $productList;
    $_SESSION['Expire'] = time() + (60 * 60 * 24 * 7);
}
//maak winkelmandje leeg na druk op de knop of 1 week wachttijd
if (isset($_POST['Empty']) || time() > $_SESSION['Expire']) {
    session_destroy();
    header("Refresh:0");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Winkel met sessions</title>
</head>
<body>
<form action="winkel.php" method="post">
    <fieldset>
        <legend>Winkel</legend>
        <div>
            <label for="Product">
                <select name="Product" id="Product">
                    <option value="Boterham">Boterham</option>
                    <option value="Pannekoek">Pannekoek</option>
                    <option value="Tomaat">Tomaat</option>
                </select>
            </label>
        </div>
    </fieldset>
    <button type="submit" name="Submit" id="submit">Leg in winkelmandje</button>
    <button type="submit" name="Empty" id="empty">Maak winkelmandje leeg</button>
</form>
<div>
    <h1>Winkelmandje</h1>
    <?php
    if (isset($_SESSION['Products'])) {
        for ($i = 0; $i < count($_SESSION['Products']); $i++) {
            echo "<span>" . $_SESSION['Products'][$i] . "</span><br>";
        }
    } else {
        echo "<span>Winkelmandje is leeg</span>";
    }
    ?>
</div>
</body>
</html>
