<?php
session_start();

$productList = array();

//product toevoegen
if (isset($_POST['AddProduct'])) {
    if (isset($_SESSION['Products'])) {
        $productList = $_SESSION['Products'];
    }
    $productList[] = $_POST['Product'];
    $_SESSION['Products'] = $productList;
    $_SESSION['Expire'] = time() + (60 * 60 * 24 * 7);
}

//maak winkelmandje leeg na druk op de knop
if (isset($_POST['Empty'])) {
    session_destroy();
    header("Refresh:0");
}

//maak winkelmandje leeg na 1 week
if (isset($_SESSION['Expire'])) {
    if (time() > $_SESSION['Expire']) {
        session_destroy();
        header("Refresh:0");
    }
}

//verwijder geselecteerd product
if (isset($_POST['RemoveProduct'])) {
    for ($i = 0; $i <= count($_SESSION['Products']); $i++) {
        if (isset($_POST["Product$i"])) {
            if ($_SESSION['Products'][$i] == $_POST["Product$i"]) {
                unset($_SESSION['Products'][$i]);

            }
        }
    }
    //array opnieuw schikken -> lege plekjes er van tussen halen
    $_SESSION['Products'] = array_values($_SESSION['Products']);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Winkel met sessions</title>
</head>
<body>
<form action="sessions_winkel.php" method="post">
    <fieldset>
        <legend>Winkel</legend>
        <div>
            <label for="Product">
                <select name="Product" id="Product">
                    <option value="Boterham">Boterham</option>
                    <option value="Pannekoek">Pannekoek</option>
                    <option value="Tomaat">Tomaat</option>
                </select>
            </label>
        </div>
    </fieldset>
    <br>
    <button type="submit" name="AddProduct" id="submit">Leg in winkelmandje</button>
    <button type="submit" name="Empty" id="empty">Maak winkelmandje leeg</button>
</form>
    <h1>Winkelmandje</h1>
    <form action="sessions_winkel.php" method="post">
        <?php
        if (isset($_SESSION['Products']) && count($_SESSION['Products']) > 0) {
            for ($i = 0; $i < count($_SESSION['Products']); $i++) {
                echo "<div>
                <label>" . $_SESSION['Products'][$i] . "</label>
                <input type='checkbox' name='Product$i' value='" . $_SESSION["Products"][$i] . "'>
                </div><br>";
            }
            echo "<button type='submit' name='RemoveProduct'>Verwijder selectie</button>";
        } else {
            echo "<span>Winkelmandje is leeg :(</span>";
        }
        ?>
    </form>
</body>
</html>
