<?php
//var_dump($_POST);

$modelState = array();

function required($key, $name, &$modelState){
        $data = stripslashes($_POST[$key]);
        $data = htmlspecialchars($data);
        $data = trim($data);
        if (empty($data)){
            $modelState["{$key}Required"] ="$name required";

    }
}
$firstName = "";
required("firstName", "First name", $modelState);
var_dump($modelState);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <fieldset>
        <legend>Account info</legend>
        <div>
            <label for="firstName">First name <span>*</span></label>
            <input type="text" name="firstName" id="firstName" temp value="<?php echo isset($_POST['firstName']) ? $_POST['firstName'] : '' ; ?>">
            <span class="error"><?php echo $modelState["firstNameRequired"]; ?></span>
        </div>
        <div>
            <label for="lastName">Last name <span>*</span></label>
            <input type="text" name="lastName" id="lastName" temp value="<?php echo $_POST['lastName']; ?>">
            <span class="error"><?php echo $modelState["lastNameRequired"]; ?></span>
        </div>
        <div>
            <label for="email">E-mail <span>*</span></label>
            <input type="email" name="email" id="email" temp value="<?php echo $_POST['email']; ?>">
            <span class="error"><?php echo $modelState["emailValid"]; ?></span>
            <span class="error"><?php echo $modelState["emailRequired"]; ?></span>
        </div>
        <div>
            <label for="password">Password <span>*</span></label>
            <input type="password" name="password" id="password" temp>
            <span class="error"><?php $modelState["passwordRequired"]; ?></span>
            <span class="error"><?php $modelState["passwordValid"]; ?></span>

        </div>
        <div>
            <label for="confirmPassword">Confirm password <span>*</span></label>
            <input type="password" name="confirmPassword" id="confirmPassword" temp>
            <span class="error"><?php echo $modelState["confirmPasswordRequired"]; ?></span>
            <span class="error"><?php $modelState["confirmPasswordValid"]; ?></span>
        </div>
    </fieldset>
    <fieldset>
        <legend>Adress data</legend>
        <div>
            <label for="adres1">Adress 1 <span>*</span></label>
            <input type="text" name="adress1" id="adres1" temp value="<?php echo $_POST['adress1']; ?>">
            <span class="error"><?php echo $modelState["adress1Required"]; ?></span>
        </div>
        <div>
            <label for="adress2">Adress 2</label>
            <input type="text" name="adress2" id="adress2" value="<?php echo $_POST['adress2']; ?>">
        </div>

        <div>
            <label for="city">City <span>*</span></label>
            <input type="text" name="city" id="city" temp value="<?php echo $_POST['city']; ?>">
            <span class="error"><?php echo $modelState["cityRequired"]; ?></span>
        </div>

        <div>
            <label for="postalCode">Postal code <span>*</span></label>
            <input type="text" name="postalCode" id="postalCode" temp>
            <span class="error"><?php echo $modelState["postalCodeRequired"]; ?></span>
        </div>
        <div>
            <label for="country">Country <span>*</span></label>
            <select name="country" id="country">
                <option value="Be">Belgium</option>
                <option value="Nl">Netherlands</option>
                <option value="Tr">Turkey</option>
            </select>
            <span class="error"><?php echo $modelState["countryRequired"]; ?></span>
        </div>
    </fieldset>
    <fieldset>
        <legend>Personal data</legend>
        <div>
            <label for="mobile">Mobile</label>
            <input type="text" name="mobile" id="mobile">
        </div>
        <div>
            <label for="birthDate">Date of birth</label>
            <input type="date" name="birthDate" id="birthDate">
        </div>
        <div>
            <label for="satisfied">How satisfied are you?</label>
            <input type="range" name="satisfied" id="satisfied">
        </div>
        <div>
            <label for="gender">Gender</label>
            <input type="radio" name="gender" value="male">male
            <input type="radio" name="gender" value="female">female
        </div>
        <div>
            <label for="department">Department</label>
            <select name="department" id="department">
                <option value="hboi">HBO 5 IT</option>
                <option value="hboa">HBO Accounting</option>
                <option value="hbos">HBO 5 Store management</option>
            </select>
        </div>
        <div>
            <label for="module">Module</label>
            <select name="module" id="module">
                <option value="A5">Programming 1</option>
                <option value="A6"></option>
                <option value="B2"></option>
            </select>
        </div>
    </fieldset>
    <button type="submit" value="register" name="action">Submit</button>
</form>
</body>
</html>
