<?php
    $controller = new \RedMind\Dialog\Controller\NoticeController();
    $view = $controller->ReadingAll();
?>
<header class="flex-container">
    <div class="flex-item-1 align-content-left">
        <a class="home" href="?uc=Home/Index">Home</a>
    </div>
    <div class="flex-item-1 align-content-right">
        <h1>Notice Index</h1>
    </div>
</header>
<main>
<div class="show-room">
    <div class="flex-container">
        <div class="flex-item-3">
            <div class="command-bar flex-container">
                <h2 class="flex-item-4 align-content-left">Notice</h2>
                <a class="flex-item-1 align-content-right" href="?uc=Notice/Inserting">Notice Inserten</a>
            </div>
            <div class="detail">
            </div>
             <div class="feedback"></div>
        </div>
        <div class="flex-item-1">
        <?php $view();?>
        </div>
    </div>
</div>
</main>