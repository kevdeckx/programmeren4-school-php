<?php
    $controller = new RedMind\Dialog\Controller\NoticeController();
    $view = $controller->ReadingAll();
?>
<header class="flex-container">
    <div class="flex-item-1 align-content-left">
        <a class="home" href="?uc=Home/Index">Home</a>
    </div>
    <div class="flex-item-1 align-content-right">
        <h1>Notice updating</h1>
    </div>
</header>
<main class="show-room flex-container">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="flex-item-4 align-content-left">
                <h2>Notice</h2>
            </div>
            <div class="flex-item-1 align-content-right">
           <button type="submit" name="uc" value="Notice/Update" form="updatingForm">Update</button>
            </div>
        </div>
        <div class="detail">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="updatingForm">
            <input type="hidden" name="Id" value="<?php echo $model->getId();?>">
        <div class="field-container">
            <label for="subject">Onderwerp</label>
            <input type="text" id="subject" name="Subject" required="required" value="<?php echo $model->getSubject();?>" />
        </div>
        <div class="field-container">
            <label for="message">Boodschap</label>
            <input type="text" id="message" name="Message" required="required" value="<?php echo $model->getMessage();?>" />
        </div>
        <div class="field-container">
            <label for="code">Code</label>
            <input type="text" id="code" name="Code" value="<?php echo $model->getCode();?>" />
        </div>
        <div class="field-container">
            <label for="start">Start</label>
            <input type="date" id="start" name="Start" value="<?php echo $model->getStart();?>" />
        </div>
        <div class="field-container">
            <label for="end">Einde</label>
            <input type="date" id="end" name="End" value="<?php echo $model->getEnd();?>" />
        </div>
        <div class="field-container">
            <label for="source">Bron</label>
            <input type="text" id="source" name="Source" value="<?php echo $model->getSource();?>"/>
        </div>
   
               </form>
    
        
        </div>
            <div class="feedback"></div>
    </div>
    <div class="flex-item-1">
        <?php $view();?>
    </div>

</main>












