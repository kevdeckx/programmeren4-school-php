<?php
    $controller = new RedMind\Dialog\Controller\NoticeController();
    $view = $controller->ReadingAll();
?>

<header class="flex-container">
    <div class="flex-item-1 align-content-left">
        <a class="home" href="?uc=Home/Index">Home</a>
    </div>
    <div class="flex-item-1 align-content-right">
        <h1>Notice reading one</h1>
    </div>
</header>
<main>
    <div class="show-room flex-container">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="flex-item-4 align-content-left">
                <h2>Notice</h2>
            </div>
            <div class="flex-item-1 align-content-right">
                
        <a href="?uc=Notice/Index">Cancel</a>
        <a href="?uc=Notice/Delete/<?php echo $model->getId();?>">Delete</a>
        <a href="?uc=Notice/Inserting">Inserting</a>
        <a href="?uc=Notice/Updating/<?php echo $model->getId();?>">Updating</a>
                
            </div>
        </div>
        <div class="detail">
   
        <div class="field-container">
            <label for="subject">Onderwerp</label>
            <input type="text" id="subject" name="subject" required="required" value="<?php echo $model->getSubject();?>" readonly/>
        </div>
        <div class="field-container">
            <label for="message">Boodschap</label>
            <input type="text" id="message" name="message" required="required" value="<?php echo $model->getMessage();?>" readonly/>
        </div>
        <div class="field-container">
            <label for="code">Code</label>
            <input type="text" id="code" name="code" value="<?php echo $model->getCode();?>" readonly/>
        </div>
        <div class="field-container">
            <label for="start">Start</label>
            <input type="date" id="start" name="start" value="<?php echo $model->getStart();?>" readonly/>
        </div>
        <div class="field-container">
            <label for="end">Einde</label>
            <input type="date" id="end" name="end" value="<?php echo $model->getEnd();?>" readonly/>
        </div>
        <div class="field-container">
            <label for="source">Bron</label>
            <input type="text" id="source" name="source" value="<?php echo $model->getSource();?>" readonly/>
        </div>
    
    
        </div>
                <div class="feedback"></div>
    </div>
    <div class="flex-item-1">
        <?php $view();?>
    </div>
</div>
</main>












