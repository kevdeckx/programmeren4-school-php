<?php
    $controller = new \RedMind\Dialog\Controller\NoticeController();
    $view = $controller->ReadingAll();
?>
<header class="flex-container">
    <div class="flex-item-1 align-content-left">
        <a class="home" href="?uc=Home/Index">Home</a>
    </div>
    <div class="flex-item-1 align-content-right">
        <h1>Notice Inserting</h1>
    </div>
</header>
<main>
<div class="show-room flex-container">
    <div class="flex-item-3">
        <div class="command-bar flex-container">
            <div class="flex-item-4 align-content-left">
                <h2>Notice</h2>
            </div>
            <div class="flex-item-1 align-content-right">
                <button type="submit" name="uc" value="Notice/Insert">
                    Annuleren
                </button>
                <button type="submit" name="uc" value="Notice/Insert" form="insertingForm">
                    Insert
                </button>
            </div>
        </div>
        <div class="detail">
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="insertingForm">
                <div class="field-container">
                    <label for="Subject">Subject</label>
                    <input type="text" name="Subject" required/>
                </div>
                <div class="field-container">
                    <label for="Code">Code</label>
                    <input type="text" name="Code"/>
                </div>
                <div class="field-container">
                    <label for="Message">Message</label>
                    <input type="text" name="Message"/>
                </div>
                <div class="field-container">
                    <label for="Start">Start</label>
                    <input type="date" name="Start"/>
                </div>
                <div class="field-container">
                    <label for="End">End</label>
                    <input type="date" name="End"/>
                </div>
                <div class="field-container">
                    <label for="Source">Source</label>
                    <input type="text" name="Source"/>
                </div>
            </form>
        </div>
         <div class="feedback"></div>
    </div>
    <div class="flex-item-1">
        <?php $view();?>
    </div>
</div>
</main>



