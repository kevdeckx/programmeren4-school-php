<?php
    namespace RedMind\Dialog\Controller;
    class NoticeController extends \ModernWays\Mvc\Controller
    {
        public function index()
        {
            // view maakt geen gebruik van model
            // dus geen nood aan closure
            return $this->view();
        }
        
        public function inserting(){
            return $this->view();
        }
  
        public function insert() {
            // model updaten gebaseerd op nieuwe input van de view
            $model = new \RedMind\Dialog\Model\Notice();
            $model->setSubject(filter_input(INPUT_POST, 'Subject', FILTER_SANITIZE_STRING));
            $model->setCode(filter_input(INPUT_POST, 'Code', FILTER_SANITIZE_STRING));
            $model->setMessage(filter_input(INPUT_POST, 'Message', FILTER_SANITIZE_STRING));
            $model->setStart(filter_input(INPUT_POST, 'Start', FILTER_SANITIZE_STRING));
            $model->setEnd(filter_input(INPUT_POST, 'End', FILTER_SANITIZE_STRING));
            $model->setSource(filter_input(INPUT_POST, 'Source', FILTER_SANITIZE_STRING));
            // insert into table
            $dal = new \RedMind\Dialog\Dal\Notice();
            $dal->setNotice($model);
            $dal->insert();
            
            return $this->view(null, "Notice/Index");
        }
        
        public function readingAll() {
            $dal = new \RedMind\Dialog\Dal\Notice();
            $notice = new \RedMind\Dialog\Model\Notice();
            $noticeBoard = new \RedMind\Dialog\Model\NoticeBoard();
            $dal->setNotice($notice);
            $dal->setNoticeBoard($noticeBoard);
            $dal->readingAll();
            return $this->view($dal->getNoticeBoard(), 
                    "Notice/ReadingAll");
        }
        
        public function readingOne($id) {
            $dal = new \RedMind\Dialog\Dal\Notice();
            $notice = new \RedMind\Dialog\Model\Notice();
            $notice->setId($id);
            $dal->setNotice($notice);
            $dal->readingOne();
            return $this->view($notice);       
            }
            
        public function delete($id){
            $dal = new \RedMind\Dialog\Dal\Notice();
            $notice = new \RedMind\Dialog\Model\Notice();
            $notice->setId($id);
            $dal->setNotice($notice);
            $dal->delete();
            return $this->view(null, "Notice/Index");
        }
        
        public function updating($id){
            $dal = new \RedMind\Dialog\Dal\Notice();
            $notice = new \RedMind\Dialog\Model\Notice();
            $notice->setId($id);
            $dal->setNotice($notice);
            $dal->readingOne();
            return $this->view($notice, 'Notice/Updating');
        }
        
        public function update(){
           
            $model = new \RedMind\Dialog\Model\Notice();
            //Id........ beware of copy pasta
           
            
            $model->setId(filter_input(INPUT_POST, 'Id', FILTER_SANITIZE_STRING));
            $model->setSubject(filter_input(INPUT_POST, 'Subject', FILTER_SANITIZE_STRING));
            $model->setCode(filter_input(INPUT_POST, 'Code', FILTER_SANITIZE_STRING));
            $model->setMessage(filter_input(INPUT_POST, 'Message', FILTER_SANITIZE_STRING));
            $model->setStart(filter_input(INPUT_POST, 'Start', FILTER_SANITIZE_STRING));
            $model->setEnd(filter_input(INPUT_POST, 'End', FILTER_SANITIZE_STRING));
            $model->setSource(filter_input(INPUT_POST, 'Source', FILTER_SANITIZE_STRING));
            $dal = new \RedMind\Dialog\Dal\Notice();
            $dal->setNotice($model);
            $dal->update();
            return $this->view(null, 'Notice/Index');
        }
        
       
    }
